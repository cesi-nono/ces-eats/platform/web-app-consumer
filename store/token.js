export const state = () => ({
  access_token: null, // JWT access token
  refresh_token: null, // JWT refresh token
  //id: null, // user id
  //email_address: null, // user email address
})

export const mutations = {
  storeAccessTokenInLocalStorage(accessToken) {
    state.access_token = accessToken
  },
  getRefreshTokenFromLocalStorage() {
    return state.refreshToken
  },
  refreshTokenfunction() {
    const refreshToken = getRefreshTokenFromLocalStorage()

    this.$axios
      .post('/refreshToken', {
        refreshToken,
      })
      .then((response) => {
        const newAccessToken = response.data.accessToken
        storeAccessTokenInLocalStorage(newAccessToken)
      })
      .catch((error) => {
        alertFailed(error)
      })
  },
}
