export const state = () => ({
  restaurant: {},
  items: [],
})

export const mutations = {
  set(state, restaurant) {
    state.restaurant = restaurant
  },

  add(state, item) {
    if (state.items.map((item) => item._id).includes(item._id)) {
      const index = state.items.map((item) => item._id).indexOf(item._id)
      state.items[index].quantity++
    } else {
      state.items.push({
        ...item,
        quantity: 1,
      })
    }
  },

  increment(state, itemId) {
    const index = state.items.map((item) => item._id).indexOf(itemId)
    state.items[index].quantity++
  },

  decrement(state, itemId) {
    const index = state.items.map((item) => item._id).indexOf(itemId)
    if (state.items[index].quantity > 1) {
      state.items[index].quantity--
    }
  },

  remove(state, itemId) {
    state.items.splice(state.items.map((item) => item._id).indexOf(itemId), 1)
  },

  clear(state) {
    state.items = []
  },
}
